library(edgeR)
library(data.table)
library(WriteXLS)
library(Rsubread)
library(tidyverse)


# List files --------------------------------------------------------------

files <- list.files("../2017_9_18_marina_rnaseq/analysis/alignment", 
                    pattern = ".sorted.bam$", full.names = T)
files <- c(files, list.files("../2019_4_9_marina_rnaseq/analysis/alignment", 
                             pattern = ".sorted.bam$", full.names = T))
files2 <- gsub(".sorted.bam", "", files)
files2 <- gsub(".+/", "", files2)


# Obtain strand specific read counts -------------------------------------------

counts <- featureCounts(files = files,
                        annot.ext = "../2017_9_18_marina_rnaseq/analysis/alignment/reference/combined_annotation_modified.gff3",
                        isGTFAnnotationFile = T,
                        GTF.attrType = "ID",
                        strandSpecific = 1,
                        isPairedEnd = c(rep(T, 8), rep(F, 4)),
                        nthreads = 5)


WriteXLS(as.data.frame(counts),
         "exported_tables/raw_counts.xlsx",
         row.names = T)

# Summary stats plot of numbers of mapped reads ---------------------------

countsStat <- counts$stat[-c(3:9,11),]
colnames(countsStat) <- c("Status", files2)
countsStat <- gather(countsStat, key = Sample, value = Counts, 
                     MB_GX_fis_dps_EE_RNA_BR1:MB_OX_fis_dps_TS_RNA_BR2)
p <- ggplot(countsStat, aes(x = Status, y = Counts, color = Status, fill = Status))+
  geom_bar(stat = "identity")+
  facet_wrap(~Sample)+
  theme_minimal()+
  theme(axis.text.x=element_blank(),
        axis.ticks.x=element_blank())
ggsave("figures/stats_strand_specific_counts.png", 
       p, 
       device = "png", 
       width = 12.5, 
       height = 12,
       units = "in")
countsAnn <- counts$annotation
counts <- counts$counts
colnames(counts) <- files2



# Obtain read counts with strand specificity reversed as a check -------------------------------------------
countsRev <- featureCounts(files = files,
                           annot.ext = "../2017_9_18_marina_rnaseq/analysis/alignment/reference/combined_annotation_modified.gff3",
                           isGTFAnnotationFile = T,
                           GTF.attrType = "ID",
                           strandSpecific = 2,
                           isPairedEnd = c(rep(T, 8), rep(F, 4)),
                           requireBothEndsMapped = F,
                           nthreads = 3)

countsRevStat <- countsRev$stat[-c(3:9,11),]
colnames(countsRevStat) <- c("Status", files2)
countsRevStat <- gather(countsRevStat, key = Sample, value = Counts, 
                        MB_GX_fis_dps_EE_RNA_BR1:MB_OX_fis_dps_TS_RNA_BR2)
p <- ggplot(countsRevStat, aes(x = Status, y = Counts, color = Status, fill = Status))+
  geom_bar(stat = "identity")+
  facet_wrap(~Sample)+
  theme_minimal()+
  theme(axis.text.x=element_blank(),
        axis.ticks.x=element_blank())
ggsave("figures/stats_strand_specificity_reversed_counts.png", 
       p, 
       device = "png", 
       width = 12.5, 
       height = 12,
       units = "in")
countsRevAnn <- countsRev$annotation
countsRev <- countsRev$counts
colnames(countsRev) <- files2

# plotting strand specific counts vs reverse strand specific counts
counts2 <- cbind(id = rownames(counts), as.data.frame(counts, row.names = NA))
counts2 <- gather(counts2, key = sample, value = counts, MB_GX_fis_dps_EE_RNA_BR1:MB_OX_fis_dps_TS_RNA_BR2)

countsRev2 <- cbind(id = rownames(countsRev), as.data.frame(countsRev, row.names = NA))
countsRev2 <- gather(countsRev2, key = sample, value = counts, MB_GX_fis_dps_EE_RNA_BR1:MB_OX_fis_dps_TS_RNA_BR2)

counts3 <- cbind(counts2, revcounts = countsRev2$counts)

p <- ggplot(counts3, aes(x = counts, y = revcounts))+
  geom_point(shape = 1)+
  facet_wrap(~sample)+
  theme_minimal()+
  coord_cartesian(xlim = c(0, 3000),
                  ylim = c(0, 3000))
ggsave("figures/strand_specific_vs_reverse_strand_specific_counts.png", p,
       width = 12.5, height = 12, units = "in", dpi = 300)


# Obtain counts per feature -----------------------------------------------

### Loading annotations ###
ann <- fread("../2017_9_18_marina_rnaseq/analysis/alignment/reference/combined_annotation.gff3", 
             sep = "\t", header = F, skip = 5)
ann <- as.data.frame(ann)
colnames(ann) <- c("Sequence", "Source", "Feature", "Start", "End",
                   "Score", "Strand", "Frame", "Attributes")

grep2 <- function(pattern, x) {
  if (length(grep(pattern, x)) == 0) {
    return("NA")
  } else {
    return(grep(pattern, x))
  }
}

temp <- strsplit(ann$Attributes, split = ";")
temp2 <- NULL
for (i in 1:length(temp)) {
  temp3 <- temp[[i]]
  temp4 <- temp3[grep2("ID=", temp3)]
  temp5 <- temp3[grep2("Name=", temp3)]
  temp6 <- temp3[grep2("Alias=", temp3)]
  temp7 <- temp3[grep2("description=", temp3)]
  temp8 <- temp3[grep2("Note=", temp3)]
  temp9 <- c(temp4, temp5, temp6, temp7, temp8)
  temp2 <- rbind(temp2, temp9)
}
rownames(temp2) <- NULL
temp2 <- as.data.frame(temp2)
colnames(temp2) <- c("ID", "Name", "Alias", "description", "Note")
temp2$ID <- gsub("ID=", "", temp2$ID)
temp2$Name <- gsub("Name=", "", temp2$Name)
temp2$Alias <- gsub("Alias=", "", temp2$Alias)
temp2$description <- gsub("description=", "", temp2$description)
temp2$Note <- gsub("Note=", "", temp2$Note)

ann <- cbind(ann, temp2)
rownames(ann) <- ann$ID

# vector of gene lengths = (gene end - gene start) + 1
temp <- (ann$End-ann$Start)+1
ann <- cbind(ann, geneLengths = temp)
ann <- ann[rownames(counts), ]
WriteXLS(ann, "exported_tables/annotation.xls")

### Loading annotations done ###
# Not calculating TPMs. I've no use for it.
### TPMs
# READ-IN into this sesstion the Rscript counts_to_tpm_Rscript.R FIRST!!!
# Median insert size from insert size metrics files
# insertSizeFiles <- list.files("../", pattern = "insert_size_metrics.txt$")
# insertSizes <- NULL
# for (i in insertSizeFiles) {
#   temp <- readLines(paste0("../", i))[8]
#   temp <- as.numeric(gsub("\t.*", "", temp))
#   insertSizes <- c(insertSizes, temp)
# }
# names(insertSizes) <- colnames(counts)
# # read in the function from counts_to_tpm_Rscript.R first!!!
# # Genes smaller than mean fragment length are removed!
# tpms <- as.data.frame(counts_to_tpm(as.matrix(counts), countsAnn$Length, insertSizes))
# WriteXLS(tpms,
#          "exported_tables/tpms.xlsx",
#          row.names = T)

# Make groups
group <- rep(c("MB_GX_fis_dps_EE", "MB_GX_fis_dps_TS", 
               "MB_WT_fis_dps_EE", "MB_WT_fis_dps_TS", 
               "MB_OX_fis_dps_EE", "MB_OX_fis_dps_TS"),
             each = 2)
dge <- DGEList(counts = counts, group = group, genes = rownames(counts))
dge <- calcNormFactors(dge)

# MDS plot # GX_EE seems to have the worst correlation among replicates
pdf("figures/plotMDS.pdf", width = 10, height = 7)
par(mar = c(4,4,1,1))
plotMDS(dge, cex = 0.5)
dev.off()

## Get cpms
cpms <- cpm(dge)


# Filtering out genes that are not expressed in all samples
# Genes are kept if their cpms are >= 10 in at least two samples, i.e the rowSum of cpms(dge)>10 (a T / F matrix) is >= 2
# cpm >= 1 cutoff is too low; using >= 10
keep <- rowSums(cpms > 10) >= 2
dge <- dge[keep, , keep.lib.sizes = F]
dge <- estimateDisp(dge)

# read in the set of comparisons to make
comparisons <- read.delim("comparisons.csv", header = F, stringsAsFactors = F)

### Perform the tests
# the exactTestList will hold fold changes of all genes as determined by exactTest
exactTestList <- list()
for (i in 1:nrow(comparisons)) {
  exactTestList[[i]] <- exactTest(dge, pair = as.vector(unlist(comparisons[i,1:2])))
}
names(exactTestList) <- comparisons[,3]

exactTestTable <- NULL
for (i in 1:length(exactTestList)) {
  exactTestTable <- cbind(exactTestTable, exactTestList[[i]]$table$logFC)
}
colnames(exactTestTable) <- names(exactTestList)
rownames(exactTestTable) <- rownames(exactTestList[[1]]$table)
WriteXLS(as.data.frame(exactTestTable),
         "exported_tables/exactTestTable_all_genes_log2FCs_all_comparisons.xlsx",
         row.names = T)

# the decideTestsList will hold a vector of 0 and 1 / -1 indicating whether a gene is up / down or not mis regulated
# FDR cutoff of 0.05 (default)
decideTestsList <- lapply(exactTestList, 
                          decideTestsDGE, 
                          adjust.method = "BH")
# No adjustment of P values. Default cutoff of 0.05.
decideTestsList2 <- lapply(exactTestList,
                           decideTestsDGE,
                           adjust.method = "none")

# simple stacked barplot summary of numbers of de genes
temp <- lapply(decideTestsList, summary)
counts_of_de_genes <- NULL
for (i in 1:length(temp)) {
  counts_of_de_genes <- rbind(counts_of_de_genes, c(names(temp)[i],
                                                    "Up",
                                                    temp[[i]][3,]))
  counts_of_de_genes <- rbind(counts_of_de_genes, c(names(temp)[i],
                                                    "Down",
                                                    temp[[i]][1,]))
}
colnames(counts_of_de_genes) <- c("sample", "de", "count")
counts_of_de_genes <- as.data.frame(counts_of_de_genes)
counts_of_de_genes$count <- as.numeric(as.vector(unlist(counts_of_de_genes$count)))

ggplot(counts_of_de_genes, aes(x = sample, y = count, group = de, fill = de)) +
  geom_bar(stat = "identity", position = "dodge") +
  scale_y_log10() +
  scale_fill_manual(values = c("black", "darkgrey")) +
  xlab("") +
  ylab("Counts") +
  theme(panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.background = element_blank(),
        axis.line = element_line(),
        axis.text = element_text(size = 12),
        axis.title = element_text(size = 14),
        strip.background = element_blank(),
        legend.key = element_blank(),
        legend.title = element_blank(),
        legend.text = element_text(size = 13))  
ggsave("figures/counts_of_de_genes.svg", device = "svg", width = 8, height = 5, units = "in")

## Toptags list of all fold changes of all genes with FDR
topTagsList <- list()
for (i in 1:length(exactTestList)) {
  topTagsList[[i]] <- topTags(exactTestList[[i]], n = Inf, adjust.method = "BH")
}
topTagsList <- lapply(topTagsList, as.data.frame)
names(topTagsList) <- names(exactTestList)

# adding annotations
for (i in 1:length(topTagsList)) {
  topTagsList[[i]] <- cbind(topTagsList[[i]], ann[topTagsList[[i]][,1], c("Name", "Alias", "description")])
  rownames(topTagsList[[i]]) <- topTagsList[[i]][,1]
}

WriteXLS(topTagsList,
         ExcelFileName = "exported_tables/gene_fold_changes_with_FDR.xlsx")


# sigList: only the genes that are de according to decideTest results; i.e. below FDR 0.05
# No log2FC cutoff
sigList <- list()
for (i in 1:length(exactTestList)) {
  sigList[[i]] <- rownames(exactTestList[[i]]$table[(decideTestsList[[i]] == 1 | decideTestsList[[i]] == -1), ])
  sigList[[i]] <- topTagsList[[i]][sigList[[i]],]
  sigList[[i]] <- sigList[[i]][order(sigList[[i]]$FDR), ]
}
names(sigList) <- names(exactTestList)
WriteXLS(sigList,
         ExcelFileName = "exported_tables/significantly_de_genes_with_fdr_cutoff_point_05.xlsx")

# sigList2: same as sigList, except p.values are not FDR controlled
sigList2 <- list()
for (i in 1:length(exactTestList)) {
  sigList2[[i]] <- rownames(exactTestList[[i]]$table[(decideTestsList2[[i]] == 1 | decideTestsList2[[i]] == -1), ])
  sigList2[[i]] <- topTagsList[[i]][sigList2[[i]],]
  sigList2[[i]] <- sigList2[[i]][order(sigList2[[i]]$FDR), ]
}
names(sigList2) <- names(exactTestList)
WriteXLS(sigList2,
         ExcelFileName = "exported_tables/significantly_de_genes_with_non_controlled_pvalue_cutoff_point_05.xlsx")


## Looking at fold changes of differntially expressed genes
pdf("figures/log2FC_histogram_of_sig_de_genes_with_FDR_cutoff_point_05.pdf",
    width = 5,
    height = 5)
par(mfrow = c(2,2)) 
for (i in 1:length(sigList) ) {
  hist(sigList[[i]]$logFC, breaks = 100,
       main = names(sigList)[i],
       xlab = "log2FC")
}
par(mfrow = c(2,2)) 
for (i in 1:length(sigList) ) {
  plot(density(sigList[[i]]$logFC),
       main = names(sigList)[i],
       xlab = "log2FC")
}
dev.off()

# upsigList
upsigList <- list()
for (i in 1:length(sigList)) {
  upsigList[[i]] <- rownames(sigList[[i]][sigList[[i]]$logFC > 0,])
  upsigList[[i]] <- topTagsList[[i]][upsigList[[i]],]
  upsigList[[i]] <- upsigList[[i]][order(upsigList[[i]]$FDR), ]
}
names(upsigList) <- names(sigList)
WriteXLS(upsigList,
         ExcelFileName = "exported_tables/significantly_upregulated_genes_with_fdr_cutoff_point_05.xlsx")


# downsigList
downsigList <- list()
for (i in 1:length(sigList)) {
  downsigList[[i]] <- rownames(sigList[[i]][sigList[[i]]$logFC < 0,])
  downsigList[[i]] <- topTagsList[[i]][downsigList[[i]],]
  downsigList[[i]] <- downsigList[[i]][order(downsigList[[i]]$FDR), ]
}
names(downsigList) <- names(sigList)
WriteXLS(downsigList,
         ExcelFileName = "exported_tables/significantly_downregulated_genes_with_fdr_cutoff_point_05.xlsx")


# Getting percent of genes above log2 fold change of 1 or below -1 among sig list
sigListPerct <- NULL
for (i in 1:length(sigList)) {
  temp <- sigList[[i]]
  temp1 <- temp$logFC >= 1 | temp$logFC <= -1
  sigListPerct <- c(sigListPerct, (sum(temp1) / length(temp1) * 100)) 
}
names(sigListPerct) <- names(sigList)


# Save workspace
save.image("edgeR_workspace.RData") # thus open this workspace before using this script.

