1. Alignment folders contain only scripts used for alignment.
2. RNA-seq and ChIP-seq data can be found on the Gene Expression Omnibus (accession number: GSE152228)
3. Scripts may need to be modified with the correct file path to the files that are downloaded from GEO.
