library(data.table)
library(tidyverse)
library(ggpubr)
library(gggenes)


# Read in gene lists and function to plot coverage  --------

# Gene list
genes <- fread("../alignments/reference/combined_annotation.gff3", sep = "\t", skip = 5)

genes$alias <- gsub("ID.+Alias=", "", genes$V9)
genes$alias <- gsub(",.+", "", genes$alias)
genes$alias <- gsub("ID=", "", genes$alias)
genes$alias <- gsub(";Name=.+", "", genes$alias)
genes$alias <- gsub(";+", "", genes$alias)
genes$alias <- gsub("Note.+", "", genes$alias)
genes$start <- "temp"
genes$start[genes$V7 == "+"] <- genes$V4[genes$V7 == "+"]
genes$start[genes$V7 != "+"] <- genes$V5[genes$V7 != "+"]
genes$end <- "temp"
genes$end[genes$V7 == "+"] <- genes$V5[genes$V7 == "+"]
genes$end[genes$V7 != "+"] <- genes$V4[genes$V7 != "+"]
genes <- genes[genes$V1 == "SL1344", ]
genes <- genes[, c(1, 11, 12, 10, 7)]
colnames(genes) <- c("seqnames", "start", "end", "gene", "strand")
genes$start <- as.numeric(genes$start)
genes$end <- as.numeric(genes$end)
genes$geneMP <- rowMeans(cbind(genes$start, genes$end))
genes <- genes[-grep("misc", genes$gene), ]
genesPlus <- genes[grep("[+]", genes$strand), ]
genesMinus <- genes[grep("-", genes$strand), ]

# plotCoverages function
# Input coverage dataframe, 
# start point and end point of plot 
# and finally the named vector of colours, where the names indicate sample names
plotCoverages <- function(coverage_dataframe, startPoint, endPoint, cols) {
  plotStart <- startPoint - 2000 # 2kb upstream of start to give some perspective
  plotEnd <- endPoint + 2000 # 2kb downstream of end to give some perspective
  cols <- cols
  coverages <- coverage_dataframe
  
  # calculate max coverage in plot frame to use as box heights of boxes made to mark peaks
  maxcov <- max(coverages$cov[coverages$start >= plotStart & coverages$stop <= plotEnd])
  
  # make plot
  # coverages
  covPlot <- ggplot(coverages, aes(x = start, y = cov, fill = sample)) +
    geom_rect(data = peaks, mapping = aes(xmin = start, xmax = stop, ymin = 0, ymax = maxcov), fill = "grey90", show.legend = F, size = 0.25) +
    geom_area(show.legend = F) +
    #scale_y_continuous(limits = c(0, maxcov)) +
    #geom_rect(data = peaks, mapping = aes(xmin = start, xmax = stop, ymin = 0, ymax = maxcov, fill = NA), color = "black", show.legend = F, size = 0.25) +
    facet_wrap(~sample, ncol = 1) +
    scale_x_continuous(limits = c(plotStart, plotEnd)) +
    scale_fill_manual(values = cols) +
    theme(panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          panel.background = element_blank(),
          panel.spacing = unit(-0.25, "lines"),
          axis.title = element_blank(),
          axis.line = element_blank(),
          axis.text =  element_blank(),
          axis.ticks = element_blank(),
          strip.background = element_blank(),
          strip.text = element_blank(),
          legend.key = element_blank(),
          plot.margin = grid::unit(c(0,0,0,0), "mm"))
  
  # plus genes
  genePlot1 <- ggplot(genesPlus, aes(xmin = start, xmax = end, y = seqnames, fill = gene)) +
    geom_gene_arrow(show.legend = F, color = "black", fill = "darkgrey") +
    geom_rect(data = motifsPlus, aes(xmin = start, xmax = end, ymin = 0.95, ymax = 1.05), fill = "firebrick", color = "firebrick", alpha = 1, inherit.aes = F) +
    #geom_segment(data = tssPlus, aes(x = TSS, xend = TSS, y = 1.06, yend = 1.16), color = "black", size = 0.5, inherit.aes = F) +
    #geom_segment(data = tssPlus, aes(x = TSS, xend = TSS+100, y = 1.1528, yend = 1.1528), color = "black", size = 0.5, inherit.aes = F, arrow = arrow(length = unit(1, "mm"))) +
    scale_x_continuous(limits = c(plotStart, plotEnd)) +
    geom_text(aes(x = start, label = gene), angle = 45, nudge_y = -0.2, size = 3) +
    theme(panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          panel.background = element_blank(),
          panel.spacing = unit(0, "lines"),
          axis.ticks = element_blank(),
          axis.title = element_blank(),
          axis.text = element_blank(),
          axis.line = element_blank(),
          strip.background = element_blank(),
          strip.text = element_blank(),
          legend.key = element_blank(),
          plot.margin = grid::unit(c(0,0,0,0), "mm")
    )
  
  # minus genes
  genePlot2 <- ggplot(genesMinus, aes(xmin = start, xmax = end, y = seqnames, fill = gene)) +
    geom_gene_arrow(show.legend = F, color = "black", fill = "darkgrey") +
    geom_rect(data = motifsMinus, aes(xmin = start, xmax = end, ymin = 0.95, ymax = 1.05), fill = "firebrick", color = "firebrick", alpha = 1, inherit.aes = F) +
    #geom_segment(data = tssMinus, aes(x = TSS, xend = TSS, y = 0.94, yend = 0.84), color = "black", size = 0.5, inherit.aes = F) +
    #geom_segment(data = tssMinus, aes(x = TSS, xend = TSS-100, y = 0.8472, yend = 0.8472), color = "black", size = 0.5, inherit.aes = F, arrow = arrow(length = unit(1, "mm"))) +
    scale_x_continuous(limits = c(plotStart, plotEnd)) +
    geom_text(aes(x = start, label = gene), angle = 45, nudge_y = 0.25, size = 3) +
    theme(panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          panel.background = element_blank(),
          panel.spacing = unit(0, "lines"),
          axis.line.x = element_line(colour = "black",
                                     size = 0.5),
          axis.title.y = element_blank(),
          axis.title.x = element_blank(),
          axis.line.y = element_blank(),
          axis.text.y =  element_blank(),
          axis.ticks.y = element_blank(),
          
          strip.background = element_blank(),
          strip.text = element_blank(),
          legend.key = element_blank())
  
  # combine the plots and return
  #finalPlot <- ggarrange(covPlot, genePlot1, genePlot2, ncol = 1, nrow = 3, heights = c(10,3,3.75))
  finalPlot <- ggarrange(covPlot, genePlot1, genePlot2, ncol = 1, nrow = 3, heights = c(10,2,2.75))
  return(finalPlot)
}

# for testing function
#plotCoverages(coverages, plotStart, plotEnd, cols)

# Function to make coverage plot done ---------------------------------------------







# Read in the different datasets ------------------------------------------

# Coverage files  ---------------------------------------------------------

covFiles <- list.files("../alignments/", pattern = ".+bs10.bg")
covFiles2 <- c("GX 1", "GX 2", "GX Mock", "OX 1", "OX 2", "OX Mock", "WT 1", "WT 2", "WT Mock")
coverages <- NULL
for (i in 1:length(covFiles)) {
  temp <- fread(paste0("../alignments/", covFiles[i]))
  temp <- temp[temp$V1 == "SL1344",]
  temp$V5 <- covFiles2[i]
  coverages <- rbind(coverages, temp)
}
colnames(coverages) <- c("seqnames", "start", "stop", "cov", "sample")
unique(coverages$sample)
coverages$strain <- gsub(" .+", "", coverages$sample)
coverages$ip <- "Fis"
coverages$ip[grep("Mock", coverages$sample)] <- "Mock"
#coverages$strain[grep("Mock", coverages$sample)] <- "Mock"
coverages$sample <- factor(coverages$sample, levels = unique(coverages$sample)[c(7:9, 1:3, 4:6)])

# colours of teh coverage traces
library(scales)
cols <- c(rep("black", 2), alpha("black", alpha = 0.5),
          rep("firebrick3", 2), alpha("firebrick3", alpha = 0.5),
          rep("dodgerblue3", 2), alpha("dodgerblue3", alpha = 0.5))
names(cols) <- levels(cov$sample)


# Locations of Fis binding motifs as predicted by FIMO --------------------

motifs <- fread("../macs2_analysis/meme_results/fimo_predicted_fis_motif_occurances_in_Sal.gff", sep = "\t", skip = 1)
motifs <- motifs[, c(1,4,5, 9, 7)]
motifs <- motifs[motifs$V1 == "SL1344", ]
colnames(motifs) <- c("seqnames", "start", "end", "gene", "strand")
motifs$geneMP <- rowMeans(cbind(motifs$start, motifs$end))
motifsPlus <- motifs[motifs$strand == "+", ]
motifsMinus <- motifs[motifs$strand == "-", ]


# List of peaks for marking out on the coverage traces --------------------

peakFiles <- list.files("narrow_peaks_gff/")
peakFiles2 <- c("GX 1", "GX 2", "OX 1", "OX 2", "WT 1", "WT 2")
peaks <- NULL
for (i in 1:length(peakFiles)) {
  temp <- fread(paste0("narrow_peaks_gff/", peakFiles[i]))[, c(1,4,5)]
  temp <- cbind(temp, peakFiles2[i])
  peaks <- rbind(peaks, temp)
}
colnames(peaks) <- c("seqnames", "start", "stop", "sample")
peaks$cov <- rep(12, nrow(peaks)) # some random number, not imp, just for the cov column for plotting
peaks$sample <- factor(peaks$sample, levels = unique(peaks$sample)[c(5,6,1:4)])



# Plots -------------------------------------------------------------------


# Barplot showing binding strengths of Fis around fis (yhdG) --------

# Looks like dps gene is in its own transcription unit, and there's no detectable Fis binding near it. So only plotting fis locale here.
fis_binding_at_yhdG <- data.frame(seqnames = NULL,
                         genes = NULL,
                         folchange = NULL,
                         FDR = NULL)
for (i in 1:length(peaksDbaReportAnn)) {
  temp <- peaksDbaReportAnn[[i]]
  # Gene upstream of fis, i.e. yhdG, has Fis binding in its promoter. Most likely the yhdG gene is in an operon with fis. [CONFIRM]
  temp <- temp[grep("yhdG", temp$genes), ]
  # Take relevant columns from temp for the fis_binding_at_yhdG dataframe
  fis_binding_at_yhdG <- rbind(fis_binding_at_yhdG,
                               cbind(temp[,c(1,13,9,11)], comparison = names(peaksDbaReportAnn[i])))
}
fis_binding_at_yhdG$comparison <- c("GX_vs_OX", "GX_vs_WT", "OX_vs_WT")
rownames(fis_binding_at_yhdG) <- NULL
fis_binding_at_yhdG <- fis_binding_at_yhdG[c(2,3,1),]
fis_binding_at_yhdG$comparison <- factor(fis_binding_at_yhdG$comparison, 
                                         levels = unique(fis_binding_at_yhdG$comparison))

ggplot(fis_binding_at_yhdG, aes(x = comparison, y = Fold, fill = FDR)) +
  geom_col(position = "dodge") +
  #scale_fill_manual(values = c("firebrick", "dodgerblue")) +
  geom_hline(yintercept = 0) +
  ylab("Log2 FC") +
  scale_fill_gradientn(colors = c("red", "gray40", "gray60", "gray80"),
                        values = c(0, 0.05, 0.5, 1), 
                        breaks = c(0, 0.05, 0.5, 1)) + 
  theme(panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.background = element_blank(),
        panel.spacing = unit(2, "lines"),
        axis.line = element_line(colour = "black",
                                 size = 0.5),
        axis.line.x = element_blank(),
        axis.title.x = element_blank(),
        #legend.title = element_blank(),
        #strip.background = element_rect(fill = "gray90"),
        legend.key = element_blank())

# Drop GX_vs_OX as it's not very useful
fis_binding_at_yhdG <- fis_binding_at_yhdG[-3,]
ggplot(fis_binding_at_yhdG, aes(x = comparison, y = Fold, fill = FDR)) +
  geom_col(position = "dodge") +
  scale_fill_gradientn(colors = c("red", "gray40", "gray60", "gray80"),
                       values = c(0, 0.05, 0.5, 1), 
                       breaks = c(0, 0.05, 0.5, 1)) + 
  geom_hline(yintercept = 0) +
  ylab("Log2 FC") +
  theme(panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.background = element_blank(),
        panel.spacing = unit(2, "lines"),
        axis.line = element_line(colour = "black",
                                 size = 0.5),
        axis.line.x = element_blank(),
        axis.title.x = element_blank(),
        #legend.title = element_blank(),
        #strip.background = element_rect(fill = "gray90"),
        legend.key = element_blank())
ggsave("fis_diffBind_output/fis_binding_at_yhdg.svg",
       width = 3, height = 5, device = "svg", units = "in")



# Plot of coverages of Fis around fis (yhdG) ------------------------------

plotStart <- 3576691 # start position of yhdG gene
plotEnd <- 3577978 # end position of fis gene

coverage_around_fis <- plotCoverages(coverages, plotStart, plotEnd, cols)
ggsave(filename = "coverage_plots/coverage_around_fis.svg", plot = coverage_around_fis, device = "svg", width = 10, height = 10, units = "in")


# Region near ori ---------------------------------------------------------

plotStart <- 4310000 # start position of yhdG gene
plotEnd <- 4340000 # end position of fis gene

coverage_near_ori <- plotCoverages(coverages, plotStart, plotEnd, cols)
ggsave(filename = "coverage_plots/coverage_near_ori.svg", plot = coverage_near_ori, device = "svg", width = 10, height = 10, units = "in")


# Region near ter ---------------------------------------------------------

plotStart <- 1385000 # start position of yhdG gene
plotEnd <- 1410000 # end position of fis gene

coverage_near_ter <- plotCoverages(coverages, plotStart, plotEnd, cols)
ggsave(filename = "coverage_plots/coverage_near_ter.svg", plot = coverage_near_ter, device = "svg", width = 10, height = 10, units = "in")

