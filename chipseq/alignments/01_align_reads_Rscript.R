# bwa Version: 0.7.15-r1140
# samtools Version: 1.9 (using htslib 1.9)
# bedtools v2.27.1
# deeptools 3.3.0
files <- list.files("../../fastq/",
                    pattern = ".fastq$")

# filenames
filenames <- gsub(".fastq", "", files)

# For all fastq files align them, convert them to bam.
# bwa index the genome
cat("bwa index -a is combined_reference.fa")
cat("\n")
system("bwa index -a is combined_reference.fa")

# bwa mem
# There is some formula for choosing which bases to trim. Check the bwa manual.
for (i in 1:length(filenames)) {
  cat(paste0("bwa mem -t 3 combined_reference.fa ../../fastq/",
             files[i], " > ",
             filenames[i], ".sam"))
  cat("\n")
  system(paste0("bwa mem -t 3 combined_reference.fa ../../fastq/",
                files[i], " > ",
                filenames[i], ".sam"))
}

### Get stats before removing low mapping quality reads

# samtools flagstat
for (i in 1:length(filenames)) {
  cat(paste0("samtools flagstat ",
             filenames[i], ".sam ",
             "> ", filenames[i], ".flagstat"))
  cat("\n")
  system(paste0("samtools flagstat ",
                filenames[i], ".sam ",
                "> ", filenames[i], ".flagstat"))
}

# samtools stats
for (i in 1:length(filenames)) {
  cat(paste0("samtools stats ",
             filenames[i], ".sam ",
             "> ", filenames[i], ".stats"))
  cat("\n")
  system(paste0("samtools stats ",
                filenames[i], ".sam ",
                "> ", filenames[i], ".stats"))
}
###

# samtools: get unmapped reads
for (i in 1:length(filenames)) {
  cat(paste0("samtools view -f 4 ",
             filenames[i], ".sam ",
             "-o ", filenames[i], ".unmapped.sam"))
  cat("\n")
  system(paste0("samtools view -f 4 ",
                filenames[i], ".sam ",
                "-o ", filenames[i], ".unmapped.sam"))
}

# samtools: convert sam to bam
# mapping quality 20 set here
for (i in 1:length(filenames)) {
  cat(paste0("samtools view -b -q 20 ", 
             filenames[i], ".sam ", 
             "-o ", filenames[i], ".bam"))
  cat("\n")
  system(paste0("samtools view -b -q 20 ", 
                filenames[i], ".sam ", 
                "-o ", filenames[i], ".bam"))
}

# samtools: sort bam file
for (i in 1:length(filenames)) {
  cat(paste0("samtools sort -o ",
             filenames[i], ".sorted.bam ",
             filenames[i], ".bam"))
  cat("\n")
  system(paste0("samtools sort -o ",
                filenames[i], ".sorted.bam ",
                filenames[i], ".bam"))
}

# samtools: convert sorted bam file to sorted sam file
for (i in 1:length(filenames)) {
  cat(paste0("samtools view ",
             filenames[i], ".sorted.bam ",
             "-o ", filenames[i], ".sorted.sam"))
  cat("\n")
  system(paste0("samtools view ",
                filenames[i], ".sorted.bam ",
                "-o ", filenames[i], ".sorted.sam"))
}

# samtools index
for (i in 1:length(filenames)) {
  cat(paste0("samtools index ",
             filenames[i], ".sorted.bam"))
  cat("\n")
  system(paste0("samtools index ",
                filenames[i], ".sorted.bam"))
}

### Get stats after removing low mapping quality reads
# samtools idxstats
for (i in 1:length(filenames)) {
  cat(paste0("samtools idxstats ",
             filenames[i], ".sorted.bam ",
             "> ", filenames[i], ".sorted.idxstats"))
  cat("\n")
  system(paste0("samtools idxstats ",
                filenames[i], ".sorted.bam ",
                "> ", filenames[i], ".sorted.idxstats"))
}

# samtools flagstat
for (i in 1:length(filenames)) {
  cat(paste0("samtools flagstat ",
             filenames[i], ".sorted.bam ",
             "> ", filenames[i], ".sorted.flagstat"))
  cat("\n")
  system(paste0("samtools flagstat ",
                filenames[i], ".sorted.bam ",
                "> ", filenames[i], ".sorted.flagstat"))
}

# samtools stats
for (i in 1:length(filenames)) {
  cat(paste0("samtools stats ",
             filenames[i], ".sorted.bam ",
             "> ", filenames[i], ".sorted.stats"))
  cat("\n")
  system(paste0("samtools stats ",
                filenames[i], ".sorted.bam ",
                "> ", filenames[i], ".sorted.stats"))
}
###

#### Removing duplicate reads ####
# samtools rmdup
for (i in 1:length(filenames)) {
  cat(paste0("samtools rmdup -s ",
             filenames[i], ".sorted.bam ",
             filenames[i], ".sorted.nodups.bam"))
  cat("\n")
  system(paste0("samtools rmdup -s ",
                filenames[i], ".sorted.bam ",
                filenames[i], ".sorted.nodups.bam"))
}

### Get stats after removing duplicates ###
# samtools idxstats
for (i in 1:length(filenames)) {
  cat(paste0("samtools idxstats ",
             filenames[i], ".sorted.nodups.bam ",
             "> ", filenames[i], ".sorted.nodups.idxstats"))
  cat("\n")
  system(paste0("samtools idxstats ",
                filenames[i], ".sorted.nodups.bam ",
                "> ", filenames[i], ".sorted.nodups.idxstats"))
}

# samtools flagstat
for (i in 1:length(filenames)) {
  cat(paste0("samtools flagstat ",
             filenames[i], ".sorted.nodups.bam ",
             "> ", filenames[i], ".sorted.nodups.flagstat"))
  cat("\n")
  system(paste0("samtools flagstat ",
                filenames[i], ".sorted.nodups.bam ",
                "> ", filenames[i], ".sorted.nodups.flagstat"))
}

# samtools stats
for (i in 1:length(filenames)) {
  cat(paste0("samtools stats ",
             filenames[i], ".sorted.nodups.bam ",
             "> ", filenames[i], ".sorted.nodups.stats"))
  cat("\n")
  system(paste0("samtools stats ",
                filenames[i], ".sorted.nodups.bam ",
                "> ", filenames[i], ".sorted.nodups.stats"))
}
###
# bedtools convert .sorted.bam to bed
for (i in 1:length(filenames)) {
  cat(paste0("bedtools bamtobed -i ",
             filenames[i], ".sorted.bam ",
             "> ", filenames[i], ".sorted.bed"))
  cat("\n")
  system(paste0("bedtools bamtobed -i ",
                filenames[i], ".sorted.bam ",
                "> ", filenames[i], ".sorted.bed"))
}

# bedtools convert .sorted.nodups.bam to bed
for (i in 1:length(filenames)) {
  cat(paste0("bedtools bamtobed -i ",
             filenames[i], ".sorted.nodups.bam ",
             "> ", filenames[i], ".sorted.nodups.bed"))
  cat("\n")
  system(paste0("bedtools bamtobed -i ",
                filenames[i], ".sorted.nodups.bam ",
                "> ", filenames[i], ".sorted.nodups.bed"))
}

### Using deeptools to create bigwig files to show raw coverage traces ###
# Need to index files first
for (i in list.files(pattern = ".sorted.nodups.bam$")) {
  cat(paste0("samtools index ", i))
  cat("\n")
  system(paste0("samtools index ", i))
}

# Deeptools: calculate coverage of .sorted.bam with bs 10 and output format .bw
for (i in 1:length(filenames)) {
  file2open <- paste0(filenames[i], ".sorted.bam")
  libSize <- readLines(paste0(filenames[i], ".sorted.flagstat"))
  libSize <- libSize[grep(" 0 mapped ", libSize)]
  libSize <- as.numeric(strsplit(libSize, split = " \\+ 0 mapped")[[1]][1])
  libSize <- 1e6/libSize
  cat(paste0("bamCoverage -bs 10 -p 3 -e 150 --scaleFactor ",
             libSize,
             " -b ", file2open,
             " -o ", filenames[i], ".sorted.bw"))
  cat("\n")
  system(paste0("bamCoverage -bs 10 -p 3 -e 150 --scaleFactor ",
                libSize,
                " -b ", file2open,
                " -o ", filenames[i], ".sorted.bw"))
  
}

# Deeptools: calculate coverage of .sorted.bam with bs 10 and output format .bg
for (i in 1:length(filenames)) {
  file2open <- paste0(filenames[i], ".sorted.bam")
  libSize <- readLines(paste0(filenames[i], ".sorted.flagstat"))
  libSize <- libSize[grep(" 0 mapped ", libSize)]
  libSize <- as.numeric(strsplit(libSize, split = " \\+ 0 mapped")[[1]][1])
  libSize <- 1e6/libSize
  cat(paste0("bamCoverage -bs 10 -p 3 -of bedgraph --scaleFactor ",
             libSize,
             " -b ", file2open,
             " -o ", filenames[i], "_bs10.bg"))
  cat("\n")
  system(paste0("bamCoverage -bs 10 -p 3 -of bedgraph --scaleFactor ",
                libSize,
                " -b ", file2open,
                " -o ", filenames[i], "_bs10.bg"))
  
}

# Compile stats -----------------------------------------------------------

library(tidyverse)
library(xlsx)

stats_files <- list.files(pattern = "flagstat")
stats_files_2 <- unique(gsub("\\..+", "", stats_files))

flagstat_results <- data.frame(sample = NULL,
                               total = NULL,
                               mapped = NULL,
                               percent_mapped = NULL,
                               unmapped = NULL,
                               unique_mapped = NULL,
                               percent_unique_of_total = NULL,
                               percent_unique_of_mapped = NULL)
for (i in 1:length(stats_files_2)) {
  flagstat_file <- readLines(paste0(stats_files_2[i], ".flagstat"))
  temp1 <- as.numeric(gsub(" .+", "", flagstat_file[1]))
  temp2 <- as.numeric(gsub(" .+", "", flagstat_file[5]))
  temp3 <- as.numeric(gsub("%.+", "", (gsub(".+\\(", "", flagstat_file[5]))))
  flagstat_sorted_file <- readLines(paste0(stats_files_2[i], ".sorted.flagstat"))
  temp4 <- as.numeric(gsub(" .+", "", flagstat_sorted_file[5]))
  temp5 <- data.frame(sample = stats_files_2[i],
                      total = temp1,
                      mapped = temp2,
                      percent_mapped = temp3,
                      unmapped = temp1-temp2,
                      unique_mapped = temp4,
                      percent_unique_of_total = (temp4/temp1)*100,
                      percent_unique_of_mapped = (temp4/temp2)*100)
  flagstat_results <- rbind(flagstat_results, temp5)
}
write.xlsx(flagstat_results, "mapping_results.xlsx", row.names = F)

flagstat_results_long <- flagstat_results %>% 
  gather(total, mapped, percent_mapped, unmapped, unique_mapped, percent_unique_of_total, percent_unique_of_mapped, key = "stat", value = "stat_value")

flagstat_results_long$stat <- factor(flagstat_results_long$stat, levels = unique(flagstat_results_long$stat))

ggplot(flagstat_results_long, aes(y = stat_value, x = stat, fill = sample)) +
  facet_wrap(~stat, scales = "free", ncol = 3) +
  geom_bar(stat = "identity", position = "dodge2")
ggsave("stats_summary.pdf", device = "pdf", width = 13, height = 10)



### Cleanup to increase disk space ###
for (i in 1:length(filenames)) {
  #   system(paste0("rm ", filenames[i], ".bam"))
  system(paste0("rm ", filenames[i], ".sam"))
}

### Cleanup to increase disk space ###
# for (i in 1:length(filenames)) {
#   cat(paste0("rm ", filenames[i], ".bam"))
#   cat("\n")
#   system(paste0("rm ", filenames[i], ".bam"))
#   cat(paste0("rm ", filenames[i], ".sam"))
#   cat("\n")
#   system(paste0("rm ", filenames[i], ".sam"))
# }
