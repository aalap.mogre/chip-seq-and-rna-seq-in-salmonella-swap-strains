1. WGS data for the two strains, OX and 2xdO, can be found on the European Nucleotide Archive (accession number: ERS2515905 and ERS4653304, respectively). 
2. Reference sequence used can be found in reference_gbk_sequence
3. breseq_analysis contains only the output folders of the Breseq analysis. SNP data for each strain is contained within its own subfolder. Click on the html files within the output folder of each strain to look at the snp lists. (breseq_analysis/strain_name/output/summary.html and browse the html file).
4. Summarized excel and text files are also available, as are heatmaps.
